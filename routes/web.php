<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['verify' => true]);

Route::get('/mis-clinicas', 'HomeController@index')->middleware('verified')->name('panel-ppal');



Route::resource('clinicas', 'ClinicaController');

Route::middleware(['checkOwnership','auth'])->group(function () {

    Route::get('panel/clinica/{clinica_id}/', 'ClinicaController@panelClinica')->name('clinicas.panel');

    /******************* */
    
    Route::get('clinica/{clinica_id}/servicios'                                 ,'ServicioController@index'    )->name('servicios.index');
    Route::get('clinica/{clinica_id}/servicios/crear'                           ,'ServicioController@create'   )->name('servicios.create');
    Route::post('clinica/{clinica_id}/servicios/crear'                          ,'ServicioController@store'    )->name('servicios.store');
    Route::get('clinica/{clinica_id}/servicios/eliminar/{servicio_id}'          ,'ServicioController@destroy'  )->name('servicios.destroy');
    Route::get('clinica/{clinica_id}/servicios/ver/{servicio_id}'               ,'ServicioController@show'     )->name('servicios.show');
    Route::get('clinica/{clinica_id}/servicios/editar/{servicio_id}'            ,'ServicioController@edit'     )->name('servicios.edit');
    Route::patch('clinica/{clinica_id}/servicios/actualizar/{servicio_id}'      ,'ServicioController@update'   )->name('servicios.update');
    Route::delete('clinica/{clinica_id}/servicios/eliminar/{servicio_id}'       ,'ServicioController@destroy'  )->name('servicios.destroy');
    
    ///////////////////////////////////////

    Route::get('clinicas/{clinica_id}/propietarios'                   ,'propietarioController@index'    )->name('propietarios.index');
    Route::get('clinicas/{clinica_id}/propietarios/registrar'         ,'propietarioController@create'   )->name('propietarios.create');
    Route::post('clinicas/{clinica_id}/propietarios/registrar'        ,'propietarioController@store'    )->name('propietarios.store');
    Route::get('clinicas/{clinica_id}/propietarios/ver/{id}'          ,'propietarioController@show'     )->name('propietarios.show');
    Route::get('clinicas/{clinica_id}/propietarios/editar/{id}'       ,'propietarioController@edit'     )->name('propietarios.edit');
    Route::patch('clinicas/{clinica_id}/propietarios/editar/{id}'     ,'propietarioController@update'   )->name('propietarios.update');
    Route::delete('clinicas/{clinica_id}/propietarios/eliminar/{id}'  ,'propietarioController@destroy'  )->name('propietarios.destroy');

    /////////////////////////////////////////////////////////////////////////////////

    Route::get('clinicas/{clinica_id}/mascotas'                    ,'MascotaController@index'    )->name('mascotas.index');
    Route::get('clinicas/{clinica_id}/mascotas/registrar'          ,'MascotaController@create'   )->name('mascotas.create');
    Route::post('clinicas/{clinica_id}/mascotas/registrar'         ,'MascotaController@store'    )->name('mascotas.store');
    Route::get('clinicas/{clinica_id}/mascotas/ver/{id}'           ,'MascotaController@show'     )->name('mascotas.show');
    Route::get('clinicas/{clinica_id}/mascotas/editar/{id}'        ,'MascotaController@edit'     )->name('mascotas.edit');
    Route::patch('clinicas/{clinica_id}/mascotas/editar/{id}'      ,'MascotaController@update'   )->name('mascotas.update');
    Route::delete('clinicas/{clinica_id}/mascotas/eliminar/{id}'   ,'MascotaController@destroy'  )->name('mascotas.destroy');
});

Route::get('clinicas/{clinica_id}/Trabajadores','TrabajadoresController@index')->name('empleados.index');



Route::resource('especies','EspecieController')->middleware( 'role:god', 'auth' );



Route::resource('mascotas', 'MascotaController');