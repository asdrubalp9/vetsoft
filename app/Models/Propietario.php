<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Propietario",
 *      required={"nombre", "correo"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="correo",
 *          description="correo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telefono1",
 *          description="telefono1",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telefono2",
 *          description="telefono2",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="direccion",
 *          description="direccion",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Propietario extends Model
{
    use SoftDeletes;

    public $table = 'propietarios';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'correo',
        'clinica_id',
        'telefono1',
        'telefono2',
        'direccion'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'clinica_id' => 'integer',
        'nombre' => 'string',
        'correo' => 'string',
        'telefono1' => 'string',
        'telefono2' => 'string',
        'direccion' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:50|min:3',
        'correo' => 'required|email|unique:propietarios,correo',
        'telefono1' => 'min:3',
        'telefono2' => 'min:3',
        'direccion' => 'min:3|max:500'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function clinica(){
        return $this->belongsTo(\App\Models\Clinica::class, 'clinica_id', 'id');
    }
    
}

