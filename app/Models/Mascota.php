<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Mascota",
 *      required={"nombre", "fecha_nacimiento", "sexo", "raza", "is_sterilized"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="propietario_id",
 *          description="propietario_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="especie_id",
 *          description="especie_id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="fecha_nacimiento",
 *          description="fecha_nacimiento",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="sexo",
 *          description="sexo",
 *          type="boolean"
 *      ),
 *      @SWG\Property(
 *          property="raza",
 *          description="raza",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="color",
 *          description="color",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="is_sterilized",
 *          description="is_sterilized",
 *          type="boolean"
 *      )
 * )
 */
class Mascota extends Model
{
    use SoftDeletes;

    public $table = 'mascotas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'propietario_id',
        'especie_id',
        'nombre',
        'fecha_nacimiento',
        'sexo',
        'raza',
        'color',
        'is_sterilized'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'propietario_id' => 'integer',
        'especie_id' => 'integer',
        'nombre' => 'string',
        'sexo' => 'boolean',
        'raza' => 'string',
        'color' => 'string',
        'is_sterilized' => 'boolean'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:50|min:3',
        'fecha_nacimiento' => 'required|date',
        'sexo' => 'required|boolean',
        'raza' => 'required',
        'color' => 'sometimes',
        'is_sterilized' => 'required|boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function Clinica()
    {
        return $this->belongsTo(\App\Models\Clinica::class, 'clinica_id', 'id');
    }
    public function Propietario()
    {
        return $this->belongsTo(\App\Models\Propietario::class, 'propietario_id', 'id');
    }
    public function Especie()
    {
        return $this->belongsTo(\App\Models\Especie::class, 'especie_id', 'id');
    }

    
}
