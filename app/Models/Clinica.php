<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Clinica",
 *      required={"nombre", "correo"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="nombre",
 *          description="nombre",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="correo",
 *          description="correo",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telefono1",
 *          description="telefono1",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="telefono2",
 *          description="telefono2",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="logo_url",
 *          description="logo_url",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="pagina_web",
 *          description="pagina_web",
 *          type="string"
 *      ),
 *      @SWG\Property(
 *          property="created_at",
 *          description="created_at",
 *          type="string",
 *          format="date-time"
 *      ),
 *      @SWG\Property(
 *          property="updated_at",
 *          description="updated_at",
 *          type="string",
 *          format="date-time"
 *      )
 * )
 */
class Clinica extends Model
{
    use SoftDeletes;

    public $table = 'clinicas';
    

    protected $dates = ['deleted_at'];



    public $fillable = [
        'nombre',
        'correo',
        'telefono1',
        'telefono2',
        'logo_url',
        'pagina_web',
        'user_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'id' => 'integer',
        'nombre' => 'string',
        'correo' => 'string',
        'telefono1' => 'string',
        'telefono2' => 'string',
        'logo_url' => 'string',
        'pagina_web' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'nombre' => 'required|max:50|min:3',
        'correo' => 'required|email|unique:clinicas,correo',
        'telefono1' => 'min:3',
        'telefono2' => 'min:3',
        'logo_url' => 'image:jpg,png',
        'pagina_web' => 'sometimes|url'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id');
    }
}
