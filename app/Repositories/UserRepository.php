<?php

namespace App\Repositories;

use App\User;
use App\Repositories\BaseRepository;
use Auth;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version August 23, 2020, 5:00 pm UTC
*/

class UserRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'email',
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return User::class;
    }
    public function usersClinic(){
        return User::where([ 'id'=> Auth::user()->id ])->with('clinicas')->first();
    }
}
