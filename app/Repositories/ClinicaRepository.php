<?php

namespace App\Repositories;

use App\Models\Clinica;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Auth;
/**
 * Class ClinicaRepository
 * @package App\Repositories
 * @version August 23, 2020, 4:14 pm UTC
*/

class ClinicaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'correo',
        'telefono1',
        'telefono2',
        'pagina_web'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/

    public function model()
    {
        return Clinica::class;
    }

    public function getUserClinic(){
        return $this->find(['user_id'=>Auth::user()->id]);
    }

    public function createClinica(Request $request){
        
        $file = $request->file('logo_url');
        $extension = $file->getClientOriginalExtension();
        $path = 'images/logos/'.uniqid().'.'.$extension;
        $img = Image::make($file);
        $img->save(public_path($path));
        $input = $request->all();
        $input['logo_url'] = $path;
        $input['user_id'] = Auth::user()->id;
        return $this->create($input);
        
    }
}

