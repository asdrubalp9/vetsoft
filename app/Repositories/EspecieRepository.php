<?php

namespace App\Repositories;

use App\Models\Especie;
use App\Repositories\BaseRepository;

/**
 * Class EspecieRepository
 * @package App\Repositories
 * @version August 29, 2020, 10:33 pm UTC
*/

class EspecieRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'fecha_nacimiento',
        'sexo',
        'raza',
        'color',
        'is_sterilized'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Especie::class;
    }

    public function especieSelectData(){
        return Especie::select('id','nombre')->whereNull('deleted_at')->get()->toArray();

    }
}
