<?php

namespace App\Repositories;

use App\Models\Servicio;
use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use App\Models\Clinica;
/**
 * Class ServicioRepository
 * @package App\Repositories
 * @version August 23, 2020, 5:00 pm UTC
*/

class ServicioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'descripcion',
        'precio'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }
    
    public function getServiciosClinica(Request $request,$clinica_id){
        

        return Servicio::where(['clinica_id' => $clinica_id])->get();
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Servicio::class;
    }

}
