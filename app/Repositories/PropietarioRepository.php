<?php

namespace App\Repositories;

use App\Models\Propietario;
use App\Repositories\BaseRepository;

/**
 * Class PropietarioRepository
 * @package App\Repositories
 * @version August 28, 2020, 1:06 pm UTC
*/

class PropietarioRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'correo',
        'telefono1',
        'telefono2',
        'direccion'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Propietario::class;
    }

    public function getAllClientsByClinic( $clinica_id ){
        
        return Propietario::where(['clinica_id' => $clinica_id])->get();

    }
}
