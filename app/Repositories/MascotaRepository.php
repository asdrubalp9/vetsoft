<?php

namespace App\Repositories;

use App\Models\Mascota;
use App\Repositories\BaseRepository;

/**
 * Class MascotaRepository
 * @package App\Repositories
 * @version August 29, 2020, 10:33 pm UTC
*/

class MascotaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'nombre',
        'fecha_nacimiento',
        'sexo',
        'raza',
        'color',
        'is_sterilized'
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Mascota::class;
    }
}
