<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Clinica extends Model
{
    //
    protected $guarded = [];
    public function owners(){
        $this->belongsTo('App/User');
    }
    public function Logo()
    {
        return $this->morphOne('App\Media', 'mediable');
    }
}
