<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEspecieRequest;
use App\Http\Requests\UpdateEspecieRequest;
use App\Http\Controllers\AppBaseController;
use App\Models\Especie;
use Illuminate\Http\Request;
use Flash;
use Response;

class EspecieController extends AppBaseController
{
    /**
     * Display a listing of the Especie.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request)
    {
        /** @var Especie $especies */
        $especies = Especie::all();

        return view('especies.index')
            ->with('especies', $especies);
    }

    /**
     * Show the form for creating a new Especie.
     *
     * @return Response
     */
    public function create()
    {
        return view('especies.create');
    }

    /**
     * Store a newly created Especie in storage.
     *
     * @param CreateEspecieRequest $request
     *
     * @return Response
     */
    public function store(CreateEspecieRequest $request)
    {
        $input = $request->all();

        /** @var Especie $especie */
        $especie = Especie::create($input);

        Flash::success('Especie saved successfully.');

        return redirect(route('especies.index'));
    }

    /**
     * Display the specified Especie.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show($id)
    {
        /** @var Especie $especie */
        $especie = Especie::find($id);

        if (empty($especie)) {
            Flash::error('Especie not found');

            return redirect(route('especies.index'));
        }

        return view('especies.show')->with('especie', $especie);
    }

    /**
     * Show the form for editing the specified Especie.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        /** @var Especie $especie */
        $especie = Especie::find($id);

        if (empty($especie)) {
            Flash::error('Especie not found');

            return redirect(route('especies.index'));
        }

        return view('especies.edit')->with('especie', $especie);
    }

    /**
     * Update the specified Especie in storage.
     *
     * @param int $id
     * @param UpdateEspecieRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEspecieRequest $request)
    {
        /** @var Especie $especie */
        $especie = Especie::find($id);

        if (empty($especie)) {
            Flash::error('Especie not found');

            return redirect(route('especies.index'));
        }

        $especie->fill($request->all());
        $especie->save();

        Flash::success('Especie updated successfully.');

        return redirect(route('especies.index'));
    }

    /**
     * Remove the specified Especie from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy($id)
    {
        /** @var Especie $especie */
        $especie = Especie::find($id);

        if (empty($especie)) {
            Flash::error('Especie not found');

            return redirect(route('especies.index'));
        }

        $especie->delete();

        Flash::success('Especie deleted successfully.');

        return redirect(route('especies.index'));
    }
}
