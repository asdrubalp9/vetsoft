<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use App\Repositories\UserRepository;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    private $userRepository;

    public function __construct(UserRepository $userRepo){
        $this->userRepository = $userRepo;
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $clinicas = $this->userRepository->usersClinic();
        
        return view('home',compact('clinicas'));
    }
}
