<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateServicioRequest;
use App\Http\Requests\UpdateServicioRequest;
use App\Repositories\ServicioRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use Auth;
use App\Models\Clinica;

class ServicioController extends AppBaseController
{
    /** @var  ServicioRepository */
    private $servicioRepository;

    public function __construct(ServicioRepository $servicioRepo)
    {
        $this->servicioRepository = $servicioRepo;
    }

    /**
     * Display a listing of the Servicio.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request,Clinica $clinica_id)
    {
        
        $servicios = $this->servicioRepository->getServiciosClinica($request, $clinica_id->id);
        
        return view('servicios.index',['servicios' => $servicios, 'clinica_id' => $clinica_id->id ]);
            
    }

    /**
     * Show the form for creating a new Servicio.
     *
     * @return Response
     */
    public function create(Request $request, Clinica $clinica_id){
        
        return view('servicios.create',['clinica_id'=> $clinica_id]);
    }

    /**
     * Store a newly created Servicio in storage.
     *
     * @param CreateServicioRequest $request
     *
     * @return Response
     */
    public function store(CreateServicioRequest $request, Clinica $clinica_id)
    {
        $input = $request->all();
        $input['clinica_id'] = $clinica_id->id;
        
        //$input['clinica_id'] = Auth::user()->cli;
        $servicio = $this->servicioRepository->create($input);
        
        Flash::success('Servicio saved successfully.');

        return redirect( route( 'servicios.index' , ['clinica_id'=> $clinica_id->id ] ));
    }

    /**
     * Display the specified Servicio.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Clinica $clinica_id ,$servicio_id)
    {
        
        $servicio = $this->servicioRepository->find($servicio_id);
        
        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index', [ 'clinica_id'=> $clinica->id]));
        }

        return view('servicios.show',['servicio'=> $servicio, 'clinica_id' => $clinica_id->id]);
    }

    /**
     * Show the form for editing the specified Servicio.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(Clinica $clinica_id,$servicio_id)
    {
        $servicio = $this->servicioRepository->find($servicio_id);
        
        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index', [ 'clinica_id'=> $clinica->id]));
        }

        return view('servicios.edit',['servicio' =>  $servicio, 'clinica_id' => $clinica_id->id ]);
    }

    /**
     * Update the specified Servicio in storage.
     *
     * @param int $id
     * @param UpdateServicioRequest $request
     *
     * @return Response
     */
    public function update( UpdateServicioRequest $request, Clinica $clinica_id, $servicio_id)
    {
        
        $servicio = $this->servicioRepository->find($servicio_id);
        
        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index', [ 'clinica_id'=> $clinica->id]));
        }

        $servicio = $this->servicioRepository->update($request->all(),$servicio_id);

        Flash::success('Servicio updated successfully.');
        
        return redirect( route('servicios.index', [ 'clinica_id'=> $clinica_id->id ] ) );
    }

    /**
     * Remove the specified Servicio from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy(Clinica $clinica_id, $servicio_id){

        
        $servicio = $this->servicioRepository->find($servicio_id);

        if (empty($servicio)) {
            Flash::error('Servicio not found');

            return redirect(route('servicios.index',['clinica_id' => $clinica_id->id]));
        }

        $this->servicioRepository->delete($servicio_id);

        Flash::success('Servicio deleted successfully.');

        return redirect(route('servicios.index',['clinica_id'=> $clinica_id->id]));
    }
}
