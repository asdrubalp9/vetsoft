<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreatePropietarioRequest;
use App\Http\Requests\UpdatePropietarioRequest;
use App\Repositories\PropietarioRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Response;
use App\Models\CLinica;

class PropietarioController extends AppBaseController
{
    /** @var  PropietarioRepository */
    private $propietarioRepository;

    public function __construct(PropietarioRepository $propietarioRepo)
    {
        $this->propietarioRepository = $propietarioRepo;
    }


    

    /**
     * Display a listing of the Propietario.
     *
     * @param Request $request
     *
     * @return Response
     */
    public function index(Request $request,Clinica $clinica_id){
        
        $propietarios = $this->propietarioRepository->getAllClientsByClinic( $clinica_id->id);
        
        return view('propietarios.index',[ 'propietarios'=> $propietarios, 'clinica_id' => $clinica_id->id  ]);
            
    }

    /**
     * Show the form for creating a new Propietario.
     *
     * @return Response
     */
    public function create(Clinica $clinica_id)
    {
        return view('propietarios.create', ['clinica_id' => $clinica_id->id]);
    }

    /**
     * Store a newly created Propietario in storage.
     *
     * @param CreatePropietarioRequest $request
     *
     * @return Response
     */
    public function store(CreatePropietarioRequest $request, Clinica $clinica_id)
    {
        $input = $request->all();
        $input['clinica_id'] = $clinica_id->id;
        $propietario = $this->propietarioRepository->create($input);

        Flash::success('Propietario saved successfully.');

        return redirect(route('propietarios.index', ['clinica_id' => $clinica_id->id] ));
    }

    /**
     * Display the specified Propietario.
     *
     * @param int $id
     *
     * @return Response
     */
    public function show(Clinica $clinica_id, $id)
    {
        $propietario = $this->propietarioRepository->find($id);

        if (empty($propietario)) {
            Flash::error('Propietario not found');

            return redirect(route('propietarios.index'))->with('clinica_id',$clinica_id->id);
        }

        return view('propietarios.show',['propietario'=> $propietario, 'clinica_id'=>$clinica_id]);
    }

    /**
     * Show the form for editing the specified Propietario.
     *
     * @param int $id
     *
     * @return Response
     */
    public function edit(Clinica $clinica_id, $id)
    {
        $propietario = $this->propietarioRepository->find($id);

        if (empty($propietario)) {
            Flash::error('Propietario not found');

            return redirect(route('propietarios.index'))->with('clinica_id',$clinica_id->id);
        }

        return view('propietarios.edit',['propietario'=> $propietario,'clinica_id'=>$clinica_id]);
    }

    /**
     * Update the specified Propietario in storage.
     *
     * @param int $id
     * @param UpdatePropietarioRequest $request
     *
     * @return Response
     */
    public function update(Clinica $clinica_id, UpdatePropietarioRequest $request, $id)
    {
        
        $propietario = $this->propietarioRepository->find($id);

        if (empty($propietario)) {
            Flash::error('Propietario not found');

            return redirect(route('propietarios.index'))->with('clinica_id',$clinica_id->id);
        }

        $propietario = $this->propietarioRepository->update($request->all(), $id);

        Flash::success('Propietario updated successfully.');

        return redirect(route('propietarios.index', [ 'clinica_id'=> $clinica_id->id]));
    }

    /**
     * Remove the specified Propietario from storage.
     *
     * @param int $id
     *
     * @throws \Exception
     *
     * @return Response
     */
    public function destroy(Clinica $clinica_id, $id)
    {
        $propietario = $this->propietarioRepository->find($id);

        if (empty($propietario)) {
            Flash::error('Propietario not found');

            return redirect(route('propietarios.index'))->with('clinica_id',$clinica_id->id);
        }

        $this->propietarioRepository->delete($id);

        Flash::success('Propietario deleted successfully.');

        return redirect(route('propietarios.index', ['clinica_id' => $clinica_id->id] ));
    }
}
