<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class checkOwnership

{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (  $request->clinica_id->user_id != Auth::user()->id) {
            
            return redirect( route('panel-ppal') );
        }
        
        return $next($request);
    }
}
