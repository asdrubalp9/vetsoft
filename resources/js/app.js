require('./bootstrap');

const Toast = Swal.mixin({
    toast: true,
    position: 'bottom-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})


const Alert = Swal.mixin({
    showConfirmButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    backdrop: `
    rgb(17 17 17 / 40%)
    `
})

Vue.component('tareas', require('./components/TareasComponent.vue').default);

const app = new Vue({
    el: '#app',
});