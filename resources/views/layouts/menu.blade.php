<li class="{{ Request::is('/') ? 'active' : '' }}">
    <a class="nav-link" href="{{ route('panel-ppal') }}">
        <i class="fas fa-notes-medical"></i> Panel principal
    </a>
</li>
<li class="{{ Request::is('historias*') ? 'active' : '' }}">
    <a class="nav-link" href="#">
        <i class="fas fa-notes-medical"></i> Historias
    </a>
</li>
<li class="{{ Request::is('citas*') ? 'active' : '' }}">
    <a class="nav-link" href="#">
        <i class="far fa-calendar-check"></i> Citas
    </a>
</li>
<li class="{{ Request::is('propietarios*') ? 'active' : '' }}">
    <a class="nav-link" href="#">
        <i class="fas fa-user-friends"></i> Propietarios
    </a>
</li>
<li class="{{ Request::is('animales*') ? 'active' : '' }}">
    <a class="nav-link" href="#">
        <i class="fas fa-paw"></i> Animales
    </a>
</li>



<li class="{{ Request::is('especies*') ? 'active' : '' }}">
    <a href="{{ route('especies.index') }}"><i class="fa fa-edit"></i><span>Especies</span></a>
</li>

<li class="{{ Request::is('mascotas*') ? 'active' : '' }}">
    <a href="{{ route('mascotas.index') }}"><i class="fa fa-edit"></i><span>Mascotas</span></a>
</li>

