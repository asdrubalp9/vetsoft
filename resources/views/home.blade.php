@extends('layouts.app')

@section('content')
<div class="container-fluid ">
    <div class="row mt-2">
        @if( count( $clinicas->clinicas ) > 0)
            @foreach($clinicas->clinicas as $clinica)
                <div class="col-md-4">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                {{ $clinica->nombre }}
                            </h3>
                            <!--                             
                            <div class="box-tools pull-right">
                                <span class="label label-primary">
                                    Label
                                </span>
                            </div> -->
                        </div>
                        
                        <div class="box-body">
                            The body of the box
                        </div>
                        
                        <div class="box-footer">
                            <div class="btn-group mx-auto">
                                <a class="btn btn-info" href="{{ route('clinicas.panel' , [ 'clinica_id'=> $clinica->id] ) }}">
                                    Ver panel
                                </a>
                                <a class="btn btn-success" href="{{ route('servicios.index' , [ 'clinica_id'=> $clinica->id] ) }}">
                                    Servicios
                                </a>
                                <a class="btn btn-primary" href="{{ route('empleados.index', [ 'clinica_id'=> $clinica->id] ) }}">
                                    Empleados
                                </a>
                                <a class="btn btn-warning" href="{{ route('clinicas.edit',[$clinica->id]) }}">
                                    Modificar clínica
                                </a>
                            </div>
                        </div>
                        
                    </div>
                </div>
            @endforeach
        @else
        <div class="row mt-4">
            <div class="col-md-4 d-block mx-auto float-none ">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title text-center">
                            Usted no tiene clinicas registradas! haga clic aqui para registrar    
                        </h3>
                        
                    </div>
                    
                    <div class="box-body">
                        <a class="btn btn-block my-3 btn-success" href="{{ route('clinicas.create') }}">
                            Registra tu primera clínica
                        </a>
                    </div>
                    
                    
                    
                </div>
            </div>
        </div>
            
        @endif

    </div>
</div>
@endsection
