<div class="table-responsive">
    <table class="table" id="mascotas-table">
        <thead>
            <tr>
                <th>Propietario Id</th>
        <th>Especie Id</th>
        <th>Nombre</th>
        <th>Fecha Nacimiento</th>
        <th>Sexo</th>
        <th>Raza</th>
        <th>Color</th>
        <th>Is Sterilized</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($mascotas as $mascota)
            <tr>
                <td>{{ $mascota->propietario_id }}</td>
            <td>{{ $mascota->especie_id }}</td>
            <td>{{ $mascota->nombre }}</td>
            <td>{{ $mascota->fecha_nacimiento }}</td>
            <td>{{ $mascota->sexo }}</td>
            <td>{{ $mascota->raza }}</td>
            <td>{{ $mascota->color }}</td>
            <td>{{ $mascota->is_sterilized }}</td>
                <td>
                    {!! Form::open(['route' => ['mascotas.destroy', $mascota->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('mascotas.show', [$mascota->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('mascotas.edit', [$mascota->id]) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
