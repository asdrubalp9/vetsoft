<!-- Propietario Id Field -->
<div class="form-group">
    {!! Form::label('propietario_id', 'Propietario Id:') !!}
    <p>{{ $mascota->propietario_id }}</p>
</div>

<!-- Especie Id Field -->
<div class="form-group">
    {!! Form::label('especie_id', 'Especie Id:') !!}
    <p>{{ $mascota->especie_id }}</p>
</div>

<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $mascota->nombre }}</p>
</div>

<!-- Fecha Nacimiento Field -->
<div class="form-group">
    {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento:') !!}
    <p>{{ $mascota->fecha_nacimiento }}</p>
</div>

<!-- Sexo Field -->
<div class="form-group">
    {!! Form::label('sexo', 'Sexo:') !!}
    <p>{{ $mascota->sexo }}</p>
</div>

<!-- Raza Field -->
<div class="form-group">
    {!! Form::label('raza', 'Raza:') !!}
    <p>{{ $mascota->raza }}</p>
</div>

<!-- Color Field -->
<div class="form-group">
    {!! Form::label('color', 'Color:') !!}
    <p>{{ $mascota->color }}</p>
</div>

<!-- Is Sterilized Field -->
<div class="form-group">
    {!! Form::label('is_sterilized', 'Is Sterilized:') !!}
    <p>{{ $mascota->is_sterilized }}</p>
</div>

