<!-- Propietario Id Field -->
<div class="form-group col-sm-6">
    @if( !empty($mascota) )
        {!! Form::label('propietario_id', 'Propietario Id:') !!}
        {!! Form::text('propietario_id', null, ['class' => 'form-control']) !!}
    @else
        {!! Form::label('propietario_id', 'Agregar Propietario') !!}
        <button class="btn btn-success">
        Agregar propietario
        </button>
    @endif
</div>

<!-- Especie Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('especie_id', 'Especie:') !!}

    {!! Form::select('select', $especies,  isset($mascotas->especie_id) ? $mascotas->especie_id : null   , ['class' => 'form-control']) !!}
</div>

<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control','maxlength' => 50,'minlength' => 3]) !!}
</div>

<!-- Fecha Nacimiento Field -->
<div class="form-group col-sm-6">
    {!! Form::label('fecha_nacimiento', 'Fecha Nacimiento:') !!}
    {!! Form::text('fecha_nacimiento', null, ['class' => 'form-control','id'=>'fecha_nacimiento']) !!}
</div>

@push('scripts')
    <script type="text/javascript">
        $('#fecha_nacimiento').datetimepicker({
            format: 'YYYY-MM-DD HH:mm:ss',
            useCurrent: true,
            sideBySide: true
        })
    </script>
@endpush

<!-- Sexo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sexo', 'Sexo:') !!}
    {!! Form::select('sexo', ['1' => 'Macho', '0' => 'Hembra'], null, ['class' => 'form-control']) !!}
</div>

<!-- Raza Field -->
<div class="form-group col-sm-6">
    {!! Form::label('raza', 'Raza:') !!}
    {!! Form::text('raza', null, ['class' => 'form-control']) !!}
</div>

<!-- Color Field -->
<div class="form-group col-sm-6">
    {!! Form::label('color', 'Color:') !!}
    {!! Form::text('color', null, ['class' => 'form-control']) !!}
</div>

<!-- Is Sterilized Field -->
<div class="form-group col-sm-6">
    {!! Form::label('is_sterilized', 'Is Sterilized:') !!}
    {!! Form::select('is_sterilized', ['1' => 'Sí', '0' => 'No'], null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('mascotas.index') }}" class="btn btn-default">Cancel</a>
</div>
