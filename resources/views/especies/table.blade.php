<div class="table-responsive">
    <table class="table" id="especies-table">
        <thead>
            <tr>
                <th>Nombre</th>
        <th>Descripcion</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($especies as $especie)
            <tr>
                <td>{{ $especie->nombre }}</td>
            <td>{{ $especie->descripcion }}</td>
                <td>
                    {!! Form::open(['route' => ['especies.destroy', $especie->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('especies.show', [$especie->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('especies.edit', [$especie->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                        </a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
