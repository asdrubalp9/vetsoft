<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $propietario->nombre }}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{{ $propietario->correo }}</p>
</div>

<!-- Telefono1 Field -->
<div class="form-group">
    {!! Form::label('telefono1', 'Telefono1:') !!}
    <p>{{ $propietario->telefono1 }}</p>
</div>

<!-- Telefono2 Field -->
<div class="form-group">
    {!! Form::label('telefono2', 'Telefono2:') !!}
    <p>{{ $propietario->telefono2 }}</p>
</div>

<!-- Direccion Field -->
<div class="form-group">
    {!! Form::label('direccion', 'Direccion:') !!}
    <p>{{ $propietario->direccion }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $propietario->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $propietario->updated_at }}</p>
</div>

