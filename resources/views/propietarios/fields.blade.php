<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control','maxlength' => 50,'minlength' => 3]) !!}
</div>

<!-- Correo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correo', 'Correo:') !!}
    {!! Form::email('correo', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono1', 'Telefono1:') !!}
    {!! Form::text('telefono1', null, ['class' => 'form-control','minlength' => 3]) !!}
</div>

<!-- Telefono2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono2', 'Telefono2:') !!}
    {!! Form::text('telefono2', null, ['class' => 'form-control','minlength' => 3]) !!}
</div>

<!-- Direccion Field -->
<div class="form-group col-sm-6">
    {!! Form::label('direccion', 'Direccion:') !!}
    {!! Form::text('direccion', null, ['class' => 'form-control','minlength' => 3,'maxlength' => 500]) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('propietarios.index',['clinica_id' => $clinica_id ]) }}" class="btn btn-default">
        Cancel
    </a>
</div>
