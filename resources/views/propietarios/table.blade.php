<div class="table-responsive">
    <table class="table" id="propietarios-table">
        <thead>
            <tr>
                <th>Nombre</th>
        <th>Correo</th>
        <th>Telefono1</th>
        <th>Telefono2</th>
        <th>Direccion</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($propietarios as $propietario)
            <tr>
                <td>{{ $propietario->nombre }}</td>
            <td>{{ $propietario->correo }}</td>
            <td>{{ $propietario->telefono1 }}</td>
            <td>{{ $propietario->telefono2 }}</td>
            <td>{{ $propietario->direccion }}</td>
                <td>
                    {!! Form::open(['route' => ['propietarios.destroy', ['id'=>$propietario->id, 'clinica_id' => $clinica_id]], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('propietarios.show', ['id'=>$propietario->id , 'clinica_id'=> $clinica_id ]  ) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('propietarios.edit', ['id'=>$propietario->id , 'clinica_id'=> $clinica_id ]  ) }}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
