@extends('layouts.app')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">{{ $clinica->nombre }} </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>


        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-6">
                <div class="info-box bg-aqua mx-auto" style="height:150px">
                    <a class="d-block btn btn-block btn-info" href="">
                        Crear Citas
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="info-box bg-green mx-auto" style="height:150px">
                    <a class="d-block btn btn-block btn-primary" href="{{ route('propietarios.index',['clinica_id' => $clinica->id ]) }}">
                        Registrar propietario
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="info-box bg-yellow mx-auto" style="height:150px">
                    <a class="d-block btn btn-block btn-primary" href="{{ route('mascotas.index',['clinica_id' => $clinica->id]) }}">
                        Registrar mascotas
                    </a>
                </div>
            </div>
            <div class="col-md-6">
                <div class="info-box bg-aqua mx-auto" style="height:150px">
                    <a class="d-block btn btn-block btn-info" href="#">
                        Crear historia
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection

