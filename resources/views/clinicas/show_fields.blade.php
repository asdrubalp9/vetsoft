<!-- Nombre Field -->
<div class="form-group">
    {!! Form::label('nombre', 'Nombre:') !!}
    <p>{{ $clinica->nombre }}</p>
</div>

<!-- Correo Field -->
<div class="form-group">
    {!! Form::label('correo', 'Correo:') !!}
    <p>{{ $clinica->correo }}</p>
</div>

<!-- Telefono1 Field -->
<div class="form-group">
    {!! Form::label('telefono1', 'Telefono1:') !!}
    <p>{{ $clinica->telefono1 }}</p>
</div>

<!-- Telefono2 Field -->
<div class="form-group">
    {!! Form::label('telefono2', 'Telefono2:') !!}
    <p>{{ $clinica->telefono2 }}</p>
</div>

<!-- Logo Url Field -->
<div class="form-group">
    {!! Form::label('logo_url', 'Logo Url:') !!}
    <p><img height="50" src="{{ $clinica->logo_url }}"></p>
</div>

<!-- Pagina Web Field -->
<div class="form-group">
    {!! Form::label('pagina_web', 'Pagina Web:') !!}
    <p>{{ $clinica->pagina_web }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{{ $clinica->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{{ $clinica->updated_at }}</p>
</div>

