<!-- Nombre Field -->
<div class="form-group col-sm-6">
    {!! Form::label('nombre', 'Nombre:') !!}
    {!! Form::text('nombre', null, ['class' => 'form-control','maxlength' => 50,'minlength' => 3]) !!}
</div>

<!-- Correo Field -->
<div class="form-group col-sm-6">
    {!! Form::label('correo', 'Correo:') !!}
    {!! Form::email('correo', null, ['class' => 'form-control']) !!}
</div>

<!-- Telefono1 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono1', 'Telefono1:') !!}
    {!! Form::text('telefono1', null, ['class' => 'form-control','minlength' => 3]) !!}
</div>

<!-- Telefono2 Field -->
<div class="form-group col-sm-6">
    {!! Form::label('telefono2', 'Telefono2:') !!}
    {!! Form::text('telefono2', null, ['class' => 'form-control','minlength' => 3]) !!}
</div>

<!-- Logo Url Field -->
<div class="form-group col-sm-6">
    {!! Form::label('logo_url', 'Logo Url:') !!}
    {!! Form::file('logo_url') !!}
</div>
<div class="clearfix"></div>

<!-- Pagina Web Field -->
<div class="form-group col-sm-6">
    {!! Form::label('pagina_web', 'Pagina Web:') !!}
    {!! Form::text('pagina_web', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('clinicas.index') }}" class="btn btn-default">Cancel</a>
</div>
