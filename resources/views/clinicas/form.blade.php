<div class="form-group">
    <label>Nombre de la clinica</label>
    <input name="nombre" class="form-control" type="text" value="{{ old('nombre',$clinica->nombre) }}" placeholder="Nombre de la clinica">
    @error('nombre')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
<div class="form-group">
    <label>Teléfono</label>
    <input name="telefono" class="form-control" type="text" value="{{ old('telefono',$clinica->telefono) }}" placeholder="Teléfono de la clinica">
    @error('telefono')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
<div class="form-group">
    <label>Correo</label>
    <input name="correo" class="form-control" type="text" value="{{ old('correo',$clinica->correo) }}" placeholder="Correo principal de la clinica">
    @error('correo')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
<div class="form-group">
    <label>Dirección</label>
    <input name="direccion" class="form-control" type="text" value="{{ old('direccion',$clinica->direccion) }}" placeholder="Dirección de la clínica" >
    @error('direccion')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
<div class="form-group">
    <label>Página web</label>
    <input name="pagina_web" class="form-control" type="text" value="{{ old('pagina_web',$clinica->pagina_web) }}" placeholder="Página web | https://mipagina.com" >
    @error('pagina_web')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
<div class="form-group">
    <label>Introducción</label>
    <textarea class="form-control" placeholder="Ingresa un parrafo describiendo esta clínica" name="resumen">{{ old('pagina_web',$clinica->pagina_web) }}</textarea>
    @error('resumen')
        <span class="invalid-feedback" role="alert">
            <strong>{{ $message }}</strong>
        </span>
    @enderror
</div>
