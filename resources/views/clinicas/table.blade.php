<div class="table-responsive">
    <table class="table" id="clinicas-table">
        <thead>
            <tr>
                <th>Nombre</th>
        <th>Correo</th>
        <th>Telefono1</th>
        <th>Telefono2</th>
        <th>Logo Url</th>
        <th>Pagina Web</th>
                <th colspan="3">Action</th>
            </tr>
        </thead>
        <tbody>
        @foreach($clinicas as $clinica)
            <tr>
                <td>{{ $clinica->nombre }}</td>
            <td>{{ $clinica->correo }}</td>
            <td>{{ $clinica->telefono1 }}</td>
            <td>{{ $clinica->telefono2 }}</td>
            <td><img height="50" src="{{ $clinica->logo_url }}"></td>
            <td>{{ $clinica->pagina_web }}</td>
                <td>
                    {!! Form::open(['route' => ['clinicas.destroy', $clinica->id], 'method' => 'delete']) !!}
                    <div class='btn-group'>
                        <a href="{{ route('clinicas.show', [$clinica->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-eye"></i>
                        </a>
                        <a href="{{ route('clinicas.edit', [$clinica->id]) }}" class='btn btn-default btn-xs'>
                            <i class="far fa-edit"></i>
                            <i class="glyphicon glyphicon-edit"></i>
                        </a>
                        {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                    </div>
                    {!! Form::close() !!}
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
