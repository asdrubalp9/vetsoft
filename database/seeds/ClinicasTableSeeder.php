<?php

use Illuminate\Database\Seeder;
use App\Models\Clinica;
class ClinicasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Clinica::create([
            'nombre' => 'Mi primera clinica',
            'user_id' => '1',
            'correo' => 'asd@asd.com',
            'telefono1' => '321',
            'telefono2' => '123',
            'logo_url' => 'images/logos/5f429a9782a80.png',
            'pagina_web' => 'https://mipagina.com',
        ]);
        Clinica::create([
            'nombre' => 'Muji clinica',
            'user_id' => 2,
            'correo' => 'qwe@qwe.com',
            'telefono1' => '321',
            'telefono2' => '123',
            'logo_url' => 'images/logos/5f429a9782a80.png',
            'pagina_web' => 'https://mipagina.com',
        ]);
    }
}
