<?php
//https://www.nigmacode.com/laravel/roles-de-usuario-en-Laravel
use Illuminate\Database\Seeder;
use App\User;
use Illuminate\Support\Facades\Hash;
Use Carbon\Carbon;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;


class DatabaseSeeder extends Seeder{

    //use HasRoles;

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $dru = User::create([
            'name' => 'Asdrubal ',
            'email' => 'asd@asd.com',
            'avatar_url' => 'images/avatar/gru.png',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123'),
        ]);
        $muji = User::create([
            'name' => 'mujikin ',
            'email' => 'ewq@ewq.com',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('123'),
        ]);

        $god = Role::create(
            ['name'=>'god'],
        );
        $adminRole = Role::create(
            ['name'=>'Admin'],
        );

        $empleado =Role::create(
            ['name'=>'Empleado'],
        );

        $especialista = Role::create(
            ['name'=>'Especialista'],
        );
        
        $permisos = [
            ['name'=>'Crear historias'],
            ['name'=>'Modificar historias'],
            ['name'=>'Eliminar historias'],
            ['name'=>'Crear propietarios'],
            ['name'=>'Modificar propietarios'],
            ['name'=>'Eliminar propietarios'],
            ['name'=>'Crear animales'],
            ['name'=>'Modificar animales'],
            ['name'=>'Eliminar animales'],
            ['name'=>'Crear clinicas'],
            ['name'=>'Modificar clinicas'],
            ['name'=>'Eliminar clinicas'],
            ['name'=>'Crear servicios'],
            ['name'=>'Modificar servicios'],
            ['name'=>'Eliminar servicios'],
            ['name'=>'Crear empleados'],
            ['name'=>'Modificar empleados'],
            ['name'=>'Eliminar empleados'],
            ['name'=>'Crear citas'],
            ['name'=>'Modificar citas'],
            ['name'=>'Eliminar citas'],
        ];
        foreach($permisos as $permiso){
            $coso = Permission::create( $permiso );
            $adminRole->givePermissionTo($coso);
        }
        $muji->givePermissionTo('Eliminar citas');
        

        $dru->assignRole($god);
        
        //$muji->assignRole($adminRole);
        $this->call(ClinicasTableSeeder::class);

        $this->call(EspecieTableSeeder::class);

        //$this->call(ServiciosTableSeeder::class);




    }
}
