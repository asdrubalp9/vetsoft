<?php

use Illuminate\Database\Seeder;
use App\Models\Especie;

class EspecieTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Especie::create([
            'nombre' => 'Canino',
        ]);

        Especie::create([
            'nombre' => 'Felino',
        ]);

        Especie::create([
            'nombre' => 'Roedor',
        ]);

        Especie::create([
            'nombre' => 'Ave',
        ]);
        Especie::create([
            'nombre' => 'Reptil',
        ]);
        Especie::create([
            'nombre' => 'Anfibio',
        ]);
    }
}
