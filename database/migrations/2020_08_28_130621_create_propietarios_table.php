<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropietariosTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('propietarios', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('clinica_id');
            $table->string('nombre');
            $table->text('correo');
            $table->text('telefono1', 15);
            $table->text('telefono2', 15);
            $table->text('direccion');
            $table->timestamps();
            $table->softDeletes();
            //$table->foreign('clinica_id')->references('id')->on('clinicas')->onDelete('CASCADE')->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('propietarios');
    }
}
