<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMascotasTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mascotas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('clinica_id')->unsigned();
            $table->integer('propietario_id')->unsigned();
            $table->integer('especie_id')->unsigned();
            $table->string('nombre');
            $table->timestamp('fecha_nacimiento');
            $table->boolean('sexo');
            $table->text('raza');
            $table->text('color');
            $table->boolean('is_sterilized');
            $table->softDeletes();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mascotas');
    }
}
