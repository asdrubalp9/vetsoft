<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Mascota;
use Faker\Generator as Faker;

$factory->define(Mascota::class, function (Faker $faker) {

    return [
        'propietario_id' => $faker->randomDigitNotNull,
        'especie_id' => $faker->randomDigitNotNull,
        'nombre' => $faker->word,
        'fecha_nacimiento' => $faker->date('Y-m-d H:i:s'),
        'sexo' => $faker->word,
        'raza' => $faker->text,
        'color' => $faker->text,
        'is_sterilized' => $faker->word
    ];
});
