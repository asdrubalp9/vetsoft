<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Clinica;
use Faker\Generator as Faker;

$factory->define(Clinica::class, function (Faker $faker) {

    return [
        'nombre' => $faker->word,
        'correo' => $faker->text,
        'telefono1' => $faker->text,
        'telefono2' => $faker->text,
        'logo_url' => $faker->word,
        'pagina_web' => $faker->text,
        'created_at' => $faker->date('Y-m-d H:i:s'),
        'updated_at' => $faker->date('Y-m-d H:i:s')
    ];
});
